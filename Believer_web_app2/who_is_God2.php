<?php
require('php/db.php');
?>

<!DOCTYPE html>
<html>
<head>
	<title>Who is God?</title>
	<!-- Metas -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap CDNs -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<style type="text/css">
.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 50px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav li:hover {
  color: #f1f1f1;
}

.sidenav body{
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


.flip-card {
  background-color: transparent;
  width: 320px;
  height: 320px;
  perspective: 1000px;
}

.flip-card-inner {
  position: relative;
  width: 320px;
  height: 320px;
  text-align: center;
  transition: transform 2s;
  transform-style: preserve-3d;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
}

.flip-card:hover .flip-card-inner {
  transform: rotateY(180deg);
  transition: 3s;
}

.flip-card-front, .flip-card-back {
  position: absolute;
  width: 320px;
  height: 320px;
  backface-visibility: hidden;
}

.flip-card-front {
  background-color: #bbb;
  color: black;
}

.flip-card-back {
  background-color: #2980b9;
  color: white;
  transform: rotateY(180deg);
}

.container1 {
  position: relative;
  width: 320px;
  margin: 0 auto;
}

.container1 img {vertical-align: middle;}

.container1 .content {
  position: absolute;
  bottom: 0;
  background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.5); /* Black background with 0.5 opacity */
  color: #f1f1f1;
  width: 304px;
  margin-left: 8px;
  padding: 3px;
}


:root {
  --input-padding-x: 1.5rem;
  --input-padding-y: .75rem;
}


* {
  box-sizing: border-box;
}

img {
  vertical-align: middle;
}

/* Position the image container (needed to position the left and right arrows) */
.container {
  position: relative;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Add a pointer when hovering over the thumbnail images */
.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 40%;
  width: auto;
  padding: 10px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* Container for image text */
.caption-container {
  text-align: center;
  background-color: #222;
  padding: 2px 16px;
  color: white;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Six columns side by side */
.column {
  float: left;
  width: 16.66%;
}

/* Add a transparency effect for thumnbail images */
.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}
/*body {
  background: #007bff;
  background: linear-gradient(to right, #0062E6, #33AEFF);
}*/

.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc;
}

.accordion:after {
  content: '\002B';
  color: #777;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2212";
}

.panel {
  padding: 0 13px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}



</style>


</head>
<body>

<div class="jumbotron" style="padding: 50px;">

<table>
		<tr>
	<table >
		<tr>
  
  <!-- Collapsible side nav -->
  <td>
  	<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <ul>
  <li><a href="landing.html">Home</a></li>
  <li><a href="who_is_jesus.html">Who is Jesus?</a></li>
  <li><a href="http://localhost:8080/the-believer/Believer_web_app2/php/salvationreg.php" 
  onclick="closeNav()">Salvation</a></li>
  <li><a href="http://localhost:8080/the-believer/Believer_web_app2/php/faithreg.php"
  onclick="closeNav()">Faith</a></li>
  <li><a href="http://localhost:8080/the-believer/Believer_web_app2/php/db_registration.php" 
  onclick="closeNav()">Who am I?</a></li>
  </ul>
</div>

<div id="main">
  
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
</div>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
  document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  document.body.style.backgroundColor = "white";
}
</script>
   
  </td>
<!-- end of nav -->

<!-- Log in modal -->

  <!-- The Modal -->
  

	
<!-- end of log in modal -->

  <td><blockquote style="text-align: center;margin-left: 450px;"><h1>#Tujue_Word</h1> </blockquote></td>
  </tr>
  </table>
</div>
</tr>


<h2 style="text-align:center">God the Father</h2>

<div class="container" style="margin-left: 7%;">
  <div class="mySlides">
  
    <!-- <img src="wp12.jpg" style="width:1110px;"> -->
     <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">He is the Creator and Owner of the entire universe</h1>
    <p class="lead">The earth is the Lord’s, and everything in it.
    The world and all its people belong to Him.<br>
    <?php  
        $query="SELECT * FROM links where id=1";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['God'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
  </p>
  </div>
  </div>
  </div>


  <div class="mySlides">
    <div class="numbertext">2 / 6</div>
    <!-- <img src="wp13.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">He is our Father & of our Saviour, Jesus Christ</h1>
    <p class="lead">For us there is only one God -- our Father. <br>
     All things came from him, and we live for him. <br> 
     And there is only one Lord -- Jesus Christ. <br>
     All things were made through him, and we also were made through him.<br>
    <?php  
        $query="SELECT * FROM links where id=2";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['God'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>

    <p class="lead">3 All praise to God, the Father of our Lord Jesus Christ. <br>
     God is our merciful Father and the source of all comfort.<br>
    2 Corinthians 1:3</p>

  </div>
</div>
  </div>

  <div class="mySlides">
    <div class="numbertext">3 / 6</div>
    <!-- <img src="wp14.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">He is the only Sovereign God <br> ~ He rules over the whole earth</h1>
    <p class="lead"> 13 Who is able to advise the Spirit of the Lord?[c] <br>
    Who knows enough to give him advice or teach him? <br>

    14 Has the Lord ever needed anyone’s advice? <br>
    Does he need instruction about what is good? <br>
    Did someone teach him what is right <br>
    or show him the path of justice? <br>

    15 No, for all the nations of the world <br>
    are but a drop in the bucket. <br>
    They are nothing more <br>
    than dust on the scales. <br>
    He picks up the whole earth <br>
    as though it were a grain of sand. <br>
    <?php  
        $query="SELECT * FROM links where id=3";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['God'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?> 
    </p>
  </div>
</div>
  </div>
    

  <div class="mySlides">
    <div class="numbertext">4 / 6</div>
    <!-- <img src="wp15.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">God is the epitome of goodness</h1>
    <p class="lead">8 Taste and see that the Lord is good;
    blessed is the one who takes refuge in him. <br> 
    <?php  
        $query="SELECT * FROM links where id=4";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['God'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>

    <p class="lead">22 Notice how God is both kind and severe <br>.....but kind to you if you continue to trust in his kindness.<br> 
    Romans 11:22</p>

    <p class="lead">28 And we know that in all things God works <br> for the good of those who love him, <br> who[i] have been called according to his purpose. <br>
    Romans 8:28</p>
  </div>
</div>
  </div>

  <div class="mySlides">
    <div class="numbertext">5 / 6</div>
    <!-- <img src="wp16.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">God is not only kind but also stern/severe</h1>
    <p class="lead">22 Notice how God is both kind and severe. <br>
      He is severe toward those who disobeyed..... <br>
      <?php  
        $query="SELECT * FROM links where id=5";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['God'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
      ?>
      </p>

    <p class="lead">24 For the Lord your God is a consuming fire, a jealous God.<br>
      Deutronomy 4:24 </p>
  </div>
</div>
  </div>
    
  <div class="mySlides">
    <div class="numbertext">6 / 6</div>
    <!-- <img src="wp17.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">God is the Almighty</h1>
    <p class="lead">4 “Where were you when I laid the earth’s foundation?
    Tell me, if you understand.
    5 Who marked off its dimensions? Surely you know!
    Who stretched a measuring line across it?
    6 On what were its footings set,
    or who laid its cornerstone—
    7 while the morning stars sang together
    and all the angels[a] shouted for joy? <br>
    <?php  
        $query="SELECT * FROM links where id=6";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['God'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
</div>
  </div>
    
  <a class="prev" onclick="plusSlides(-1)">❮</a>
  <a class="next" onclick="plusSlides(1)" style="margin-right: 18px;">❯</a>


 <div class="caption-container" style="padding-top: 0px; margin-top: 0px;">
    <p id="caption"></p>
  </div>

  <div class="row" style="width: 100%; margin-left: 2px;">
    <div class="column">
       
      <?php  
        $query="SELECT * FROM links where id=7";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['God'];
        }
        echo "<img src='".$image. "' alt='The Creator and Owner of the entire universe' height='150' width='180' onclick='currentSlide(1)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
       
      <?php  
        $query="SELECT * FROM links where id=8";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['God'];
        }
        echo "<img src='".$image. "' alt='Our Father and our Saviour's Father' height='150' width='180' onclick='currentSlide(2)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=9";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['God'];
        }
        echo "<img src='".$image. "' alt='Treasure chest of wisdom' height='150' width='180' onclick='currentSlide(3)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=10";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['God'];
        }
        echo "<img src='".$image. "' alt='Good God' height='150' width='180' onclick='currentSlide(4)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=11";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['God'];
        }
        echo "<img src='".$image. "' alt='Stern God' height='150' width='180' onclick='currentSlide(5)' class='demo cursor' />";
      ?> 
      
    </div>    
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=12";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['God'];
        }
        echo "<img src='".$image. "' alt='Almighty God' height='150' width='180' onclick='currentSlide(6)' class='demo cursor' />";
      ?> 
      
    </div>
  </div>
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>


<!--  Music videos -->
<div class="container" style="margin-left: 7%;">
<blockquote><h3>#Godly musicals</h3></blockquote>
<div class="jumbotron" style="padding: 50px;">
  <table>
    <tr>
      <td>
        <blockquote>Wonderful Merciful Savior (Official Video)<br> By Selah</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/fK6sYVQCqhs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>You Are the Living Word <br> By Fred Hammond & RFC</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/OyTYEeZdhK8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Who you say I am <br> By Hillsong Worship</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/lKw6uqtGFfo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
    </tr>


    <tr>
      <td>
      <blockquote class="accordion">The God Who Stays (Official Music Video) <br> By Matthew West</blockquote>
      <div class="panel">
        <iframe width="320" height="240" src="https://www.youtube.com/embed/QPwd_TQpsHY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      </td>
      <td>
      <blockquote class="accordion">for KING & COUNTRY <br> God Only Knows (Official Music Video)</blockquote>
      <div class="panel">
        <iframe width="320" height="240" src="https://www.youtube.com/embed/Q5cPQg3oq-o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      </td>
      <td>
      <blockquote class="accordion">THRONE<br> Mwanga Band [Official Music Video]</blockquote>
      <div class="panel">
       <iframe width="320" height="240" src="https://www.youtube.com/embed/oQ0AhEGa_LA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      </td>

    </tr>


    <tr>
      <td>
        <blockquote> Chris Tomlin <br> Good Good Father w/ Lyrics</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/-ak0OoFBw3c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Tauren Wells <br> Hills and Valleys (Acoustic Video)</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/p4rRCjrAyCs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td style="margin-left: 20px;">
        <blockquote>Great Are You Lord (Official Live Concert)<br> All Sons & Daughters</blockquote>
       <iframe width="320" height="240" src="https://www.youtube.com/embed/uHz0w-HG4iU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
    </tr>
  </table>
</div>
</div>

  <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>

</body>
</html>