<?php
require('php/db.php');
?>

<!DOCTYPE html>
<html>
<head>
	<title>Who am I?</title>
	<!-- Metas -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap CDNs -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<style type="text/css">
.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 50px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav li:hover {
  color: #f1f1f1;
}

.sidenav body{
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


.flip-card {
  background-color: transparent;
  width: 320px;
  height: 320px;
  perspective: 1000px;
}

.flip-card-inner {
  position: relative;
  width: 320px;
  height: 320px;
  text-align: center;
  transition: transform 2s;
  transform-style: preserve-3d;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
}

.flip-card:hover .flip-card-inner {
  transform: rotateY(180deg);
  transition: 3s;
}

.flip-card-front, .flip-card-back {
  position: absolute;
  width: 320px;
  height: 320px;
  backface-visibility: hidden;
}

.flip-card-front {
  background-color: #bbb;
  color: black;
}

.flip-card-back {
  background-color: #2980b9;
  color: white;
  transform: rotateY(180deg);
}

.container1 {
  position: relative;
  width: 320px;
  margin: 0 auto;
}

.container1 img {vertical-align: middle;}

.container1 .content {
  position: absolute;
  bottom: 0;
  background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.5); /* Black background with 0.5 opacity */
  color: #f1f1f1;
  width: 304px;
  margin-left: 8px;
  padding: 3px;
}


:root {
  --input-padding-x: 1.5rem;
  --input-padding-y: .75rem;
}


* {
  box-sizing: border-box;
}

img {
  vertical-align: middle;
}

/* Position the image container (needed to position the left and right arrows) */
.container {
  position: relative;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Add a pointer when hovering over the thumbnail images */
.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 40%;
  width: auto;
  padding: 10px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* Container for image text */
.caption-container {
  text-align: center;
  background-color: #222;
  padding: 2px 16px;
  color: white;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Six columns side by side */
.column {
  float: left;
  width: 16.66%;
}

/* Add a transparency effect for thumnbail images */
.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}
/*body {
  background: #007bff;
  background: linear-gradient(to right, #0062E6, #33AEFF);
}*/





</style>


</head>
<body>

<div class="jumbotron" style="padding: 50px;">

<table>
		<tr>
	<table >
		<tr>
  
  <!-- Collapsible side nav -->
  <td>
  	<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <ul>
  <li><a href="landing.html">Home</a></li>
  <li><a href="who_is_God2.html">Who is God?</a></li>
  <li><a href="who_is_jesus.html">Who is Jesus?</a></li>
  <li><a href="http://localhost:8080/the-believer/Believer_web_app2/php/salvationreg.php"
  onclick="closeNav()">Salvation</a></li>
  <li><a href="http://localhost:8080/the-believer/Believer_web_app2/php/faithreg.php"
  onclick="closeNav()">Faith</a></li>
  </ul>
</div>

<div id="main">
  
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
</div>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
  document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  document.body.style.backgroundColor = "white";
}
</script>
   
  </td>
<!-- end of nav -->

<!-- Log in modal -->

<body>





  <!-- The Modal -->
  

	
<!-- end of log in modal -->

  <td><blockquote style="text-align: center;margin-left: 450px;"><h1>#Tujue_Word</h1> </blockquote></td>
  </tr>
  </table>
</div>
</tr>


<h2 style="text-align:center">Me -> Who exactly?</h2>

<div class="container" style="margin-left: 7%;">
  <div class="mySlides">
  
    <!-- <img src="wp12.jpg" style="width:1110px;"> -->
     <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">God's masterpiece</h1>
    <p class="lead">26 Then God said, “Let us make human beings[a] in our image, to be like us. They will reign over the fish in the sea, the birds in the sky, the livestock, all the wild animals on the earth,[b] and the small animals that scurry along the ground.”<br>
    <?php  
        $query="SELECT * FROM links where id=1";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['who'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
  </div>
  </div>


  <div class="mySlides">
    <div class="numbertext">2 / 6</div>
    <!-- <img src="wp13.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">Yeepee!! I am God's child</h1>
    <p class="lead">Yet to all who did receive him, to those who believed in his name, he gave the right to become children of God<br>
    <?php  
        $query="SELECT * FROM links where id=2";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['who'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
</div>
  </div>

  <div class="mySlides">
    <div class="numbertext">3 / 6</div>
    <!-- <img src="wp14.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">Sheep of God's pasture</h1>
    <p class="lead"> Acknowledge that the Lord is God!
    He made us, and we are his.[a]
    We are his people, the sheep of his pasture. <br> 
    <?php  
        $query="SELECT * FROM links where id=3";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['who'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
</div>
  </div>
    
  <div class="mySlides">
    <div class="numbertext">4 / 6</div>
    <!-- <img src="wp15.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">I am a branch on the Vine <br>that is Christ and God is the gardener</h1>
    <p class="lead">“Yes, I am the vine; you are the branches. Those who remain in me, and I in them, will produce much fruit. <br> For apart from me you can do nothing." <br>
    <?php  
        $query="SELECT * FROM links where id=4";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['who'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?> 
    </p>
  </div>
</div>
  </div>

  <div class="mySlides">
    <div class="numbertext">5 / 6</div>
    <!-- <img src="wp16.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">God's workmanship</h1>
    <p class="lead">For we are God's handiwork, created in Christ Jesus to do good works, <br> which God prepared in advance for us to do. <br>
    <?php  
        $query="SELECT * FROM links where id=5";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['who'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
</div>
  </div>
    
  <div class="mySlides">
    <div class="numbertext">6 / 6</div>
    <!-- <img src="wp17.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">God's offspring</h1>
    <p class="lead"> For in him we live and move and have our being.’[a] As some of your own poets have said, ‘We are his offspring. <br> 
    <?php  
        $query="SELECT * FROM links where id=6";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['who'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
</div>
  </div>
    
  <a class="prev" onclick="plusSlides(-1)">❮</a>
  <a class="next" onclick="plusSlides(1)" style="margin-right: 18px;">❯</a>


 <div class="caption-container" style="padding-top: 0px; margin-top: 0px;">
    <p id="caption"></p>
  </div>

  <div class="row" style="width: 100%; margin-left: 2px;">
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=7";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['who'];
        }
        echo "<img src='".$image. "' alt='God's image' height='150' width='180' onclick='currentSlide(1)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
       
      <?php  
        $query="SELECT * FROM links where id=8";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['who'];
        }
        echo "<img src='".$image. "' alt='God's kid' height='150' width='180' onclick='currentSlide(2)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=9";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['who'];
        }
        echo "<img src='".$image. "' alt='The sheep of God's pasture' height='150' width='180' onclick='currentSlide(3)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
       
      <?php  
        $query="SELECT * FROM links where id=10";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['who'];
        }
        echo "<img src='".$image. "' alt='I am a branch on Jesus the Vine' height='150' width='180' onclick='currentSlide(4)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
       
      <?php  
        $query="SELECT * FROM links where id=11";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['who'];
        }
        echo "<img src='".$image. "' alt='Moulded by God' height='150' width='180' onclick='currentSlide(5)' class='demo cursor' />";
      ?> 
      
    </div>    
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=12";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['who'];
        }
        echo "<img src='".$image. "' alt='We exist in God only' height='150' width='180' onclick='currentSlide(6)' class='demo cursor' />";
      ?> 
      
    </div>
  </div>
</div>
<br><br>
<!--  Music videos -->
<div class="container" style="margin-left: 7%;">
<blockquote><h3>#Sermons to top it all off!!</h3></blockquote>
<div class="jumbotron" style="padding: 50px;">
  <table>
    <tr>
      <td>
        <blockquote>The late Dr Myles Munroe <br> WHO AM I</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/p-iqEfxGBBU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Joyce Meyer— Confidence In Christ — FULL Sermon 2017</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/G0dduz8ZafA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Understanding The Meaning For Your Existence | The late Dr. Myles Munroe</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/cFY2gXUT-UY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
    </tr>
    <tr>
      <td>
        <blockquote>How Do You See Yourself? - Joyce Meyer</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/NzMswoflBhE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Identity By the late Dr. Myles Munroe</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/AW3nAQI7qc8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td style="margin-left: 20px;">
        <blockquote>Relationship with Yourself by Joyce Meyer</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/ynpu62V06zM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
    </tr>
  </table>
</div>
</div>


<!--  Music videos -->
<div class="container" style="margin-left: 7%;">
<blockquote><h3>#Godly musicals</h3></blockquote>
<div class="jumbotron" style="padding: 50px;">
  <table>
    <tr>
      <td>
        <blockquote>Priceless  <br> By For King & Country</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/DL7WLyCzql8?list=RDEM0rUuXuoqAaAM7kynA8agjw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Look up child <br> By Lauren Daigle</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/7NAYz0zh_Es?list=RDEM0rUuXuoqAaAM7kynA8agjw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Who you say I am <br> By Hillsong Worship</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/lKw6uqtGFfo?list=RDEM0rUuXuoqAaAM7kynA8agjw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
    </tr>
    <tr>
      <td>
        <blockquote>Tasha Cobbs Leonard <br> You Know My Name ft. Jimi Cravity</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/t7owFiihXgg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Tauren Wells <br> Known</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/gbQ6Lfh5L14?list=RDEM0rUuXuoqAaAM7kynA8agjw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td style="margin-left: 20px;">
        <blockquote>No longer slave to Fear : Iam a Child of God <br> Chris Tomlin</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/5DckIHBF964" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
    </tr>
  </table>
</div>
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
    
</body>
</html>