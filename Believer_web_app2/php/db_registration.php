<!DOCTYPE html>
<html>
<head>
  <title>Log in</title>
  <!-- Metas -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Bootstrap CDNs -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<style type="text/css">
  /* Full-width input fields */
input[type=text], input[type=password], input[type=email]{
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

/* Set a style for all buttons */
button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
  position: relative;
}

img.avatar {
  width: 40%;
  border-radius: 50%;
}

.container {
  padding: 16px;
}

span.psw {
  float: right;
  padding-top: 16px;
}


/* The Close Button (x) */
.close {
  position: absolute;
  right: 25px;
  top: 0;
  color: #000;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: red;
  cursor: pointer;
}

/* Add Zoom Animation */
.animate {
  -webkit-animation: animatezoom 0.6s;
  animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
  from {-webkit-transform: scale(0)} 
  to {-webkit-transform: scale(1)}
}
  
@keyframes animatezoom {
  from {transform: scale(0)} 
  to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>




<body>

<!-- PHP code -->
<?php
require('db.php');
// If form submitted, insert values into the database.
if (isset($_REQUEST['username'])){
        // removes backslashes
 $firstname = stripslashes($_REQUEST['firstname']);
 $firstname = mysqli_real_escape_string($con,$firstname);
 $surname = stripslashes($_REQUEST['surname']);
 $surname = mysqli_real_escape_string($con,$surname);
 $pno = stripslashes($_REQUEST['pno']);
 $pno = mysqli_real_escape_string($con,$pno);
 $username = stripslashes($_REQUEST['username']);
        //escapes special characters in a string
 $username = mysqli_real_escape_string($con,$username); 
 $email = stripslashes($_REQUEST['email']);
 $email = mysqli_real_escape_string($con,$email);
 $password = stripslashes($_REQUEST['password']);
 $password = mysqli_real_escape_string($con,$password);
 $trn_date = date("Y-m-d H:i:s");
        echo $email;
        $query = "INSERT into `users` (username, password, email, trn_date)
VALUES ('$username', '".md5($password)."', '$email', '$trn_date')";
        $result = mysqli_query($con,$query);
        if($result){
             header("Location: http://localhost:8080/the-believer/Believer_web_app2/who_am_I.php");
        }
    }else{
    
?>
<!-- End of php code -->


<!-- Jumbotron -->
<div class="jumbotron">
  <h1 class="display-4" style="text-align: center;">Join our list of privileged users</h1><br>
  <blockquote style="text-align: center;">Would you like to register? <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#form_signup">Tap this!</a> </blockquote><br>
  <a href="login.php" class="btn btn-primary" style="text-align: center; margin-left: 525px;">
  Already registered? Click here!
  </a>
<!-- end of jumbotron -->

<!-- Sign up modal -->

  <div class="container">

  <!-- The modal -->
  <div class="modal fade" id="form_signup">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Register</h4>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form class="modal-content animate">
    <div class="imgcontainer">
      <span class="close" title="Close Modal" data-dismiss="modal">&times;</span>
      <img src="..\img\avatar.png" alt="Avatar" class="avatar">
    </div>

    <div class="container">


          <div class="form">
            <form name="registration" action="" method="post">
            <input type="text" name="firstname" placeholder="First name" required />
            <input type="text" name="surname" placeholder="Surname" required />
            <input type="text" name="pno" placeholder="Phone number" required />
            <input type="text" name="username" placeholder="Username" required />
            <input type="email" name="email" placeholder="Email" required />
            <input type="password" name="password" placeholder="Password" required />
            <input type="submit" name="submit" value="Register" class="btn btn-primary" />
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>


<!-- Validation for sign up modal -->
<script language="javascript">
function check()
{

 if(document.form_signup.fname.value=="")
  {
    alert("Please enter your first name");
  document.form_signup.fname.focus();
  return false;

  }
 
  if(document.form_signup.sname.value=="")
  {
    alert("Plese enter your surname");
  document.form_signup.sname.focus();
  return false;
  }
  if(document.form_signup.pno.value=="")
  {
    alert("Plese your phone number");
  document.form_signup.pno.focus();
  return false;
  }
  
 if(document.form_signup.email.value=="")
  {
    alert("Plese Enter your Email Address");
  document.form_signup.email.focus();
  return false;
  }

  if(document.form_signup.uname.value=="")
  {
    alert("Plese enter your username");
  document.form_signup.uname.focus();
  return false;
  }

  if(document.form_signup.psw.value=="")
  {
    alert("Plese Enter Your Password");
  document.form_signup.psw.focus();
  return false;
  } 
  if(document.form_signup.cpsw.value=="")
  {
    alert("Plese Enter Confirm Password");
  document.form_signup.cpsw.focus();
  return false;
  }
  if(document.form_signup.psw.value!=document.form_signup.cpsw.value)
  {
    alert("Confirm Password does not matched");
  document.form_signup.cpsw.focus();
  return false;
  }
  e=document.form_signup.email.value;
    f1=e.indexOf('@');
    f2=e.indexOf('@',f1+1);
    e1=e.indexOf('.');
    e2=e.indexOf('.',e1+1);
    n=e.length;

    if(!(f1>0 && f2==-1 && e1>0 && e2==-1 && f1!=e1+1 && e1!=f1+1 && f1!=n-1 && e1!=n-1))
    {
      alert("Please Enter valid Email");
      document.form_signup.email.focus();
      return false;
    }
  return true;
  }
  
</script>

<!-- End of validation for sign up modal -->
<?php } ?>

</body>
</html>